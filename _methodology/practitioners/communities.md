---
layout: default
title: Healthy communities
parent: Practitioners guide
nav_order: 3
---

# How to organize healthy and inclusive free software communities