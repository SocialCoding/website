---
layout: default
title: False entitlement
parent: Common challenges
nav_order: 2
---

# False entitlement
{: .no_toc}

{: .attention}
> This is just an example Challenge page. Idea is that each challenge follows a template, and cross-references pages where solutions are proposed.