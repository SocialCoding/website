---
layout: default
title: Patterns and practices
parent: Pattern library
nav_order: 3
---

# Best practices and patterns to improve the software development process
{: .no_toc }