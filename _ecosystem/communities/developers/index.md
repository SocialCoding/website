---
title: Social Coders
draft: true
author: CSDUMMI
layout: default
parent: Communities
has_children: false
nav_order: 3
---
# An open Social Coding Collaborative
Let's put Social Coding into pratice! We are trying to turn the principles and ideas of social coding into a collaborative community, where everyone supports everyone else.

We want to join developers, artists, designers, enthusiasts and people enjoying our software to work together in [this Codeberg organization](https://codeberg.org/developers).

Here anyone should be able to start a project, support other projects and rely on the help and support they receive from the community and organization.

This organization is open to anyone:
- Who wants to create or has a social coding project.
- Who wants to contribute to a social coding project.
- Who wants to help improve, advocate for or otherwise help Social Coding and Social Coding projects.

## Maintaining

When you are a maintainer of a project that you want to move to Social Coding, you can become a member of the organization. We invite anyone to transfer their Social Coding projects to the organization or to create a mirror. This allows for a list of all current social coding projects and mkaes it possible to easily maintain and support each other on projects by being in the same organization.

You can also start a whole new social coding project here.

Any project in the organization must be:
- licensed under a free or open source license. Preferably AGPLv3 or GPLv3.
- have an active maintainer
- be committed to the Social Coding idea and [it's principles](/principles.html)

Any Social Coding project has a maintainer, but no Social Coding project is run by just one person and there is no [Benevolent Dictator for Life](https://en.wikipedia.org/wiki/Benevolent_dictator_for_life).
When you don't want to maintain a project anymore, other members of the community and organization can be found to take over for you, either temporarily (e.g. a holiday) or permanently.

You are not tied to the projects you create and you're not alone when developing and maintaining them.

You can ask for help, feedback on ideas and anything you don't feel comfortable doing on your own. 

## Contributing

Anyone who contributes to a Social Coding project, by filling and discussing issues, creating pull requests,
reviewing code, etc. can become a member of the organization.

This means they can make changes, PRs, create branches and the like on any Social Coding repository under the Organization.
Only certain branches and tags, the `main` and the releases may be protected to only allow certain members, the maintainers, to write to them on sensitive projects.

## Helping

Not all software is coding. Actually a minority of software development is  actually about coding it. 
Much more time is spent on design, architecture, discussions, art, translating, funding and communication.

In these areas many more diverse skills are required. And we'd welcome anyone to join Social Coding and 
the [developers](https://codeberg.org/developers/) if they want to help us in our social endevours. 

## Utilizers

When you utilize software, you're often connected either to some customer services, that itself has no idea about the developers of the software you use, or you use free software where the developer does not have the time to support you.

We invite anyone who utilizes Social Coding Software to reach out to the Community and Organization or join. We cannot improve our software without feedback and because we don't want to track the people using our software, we rely on voluntary feedback to find bugs, problems and suggestions. 
