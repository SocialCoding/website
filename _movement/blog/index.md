---
layout: default
title: Blog and news
nav_order: 3
---

# Blog and news about Social Coding
{: .no_toc }

- [**Why we need Social Coding**](/movement/blog/why-social-coding): To address our challenges we must recognize that free software development revolves primarily around people and is mostly a social process.