---
layout: default
title: "Coding is Social"
parent: Blog and news
nav_order: 3
---

# Why we need Social Coding

To address our challenges we must recognize that free software development revolves primarily around people and is mostly a social process.
{: .fs-6 .fw-500 .ff-rajdhani }

## The beginnings of a movement

Welcome to the Social Coding Movement. We dedicate to improve free software development and we need your help with that. Our initiative is aimed at these audiences:

1. [**Movement**:](/movement/about-us) All passionate free software lovers that work together.
2. [**Practitioners**:](/methodology/practitioners/) Those of you dedicated to make their processes more social.
3. [**Supporters**:](/ecosystem/projects/ideationhub/design) Anyone contributing to a growing ecosystem of tools and methods.

We want to build a movement together with you. We want to help make free software shine. Stronger community projects, more collaboration, tighter bonds.

[**Building better together**](/principles). That is what we dedicate to. We are Social Coders and we are coding social. If this appeals to you, then don't hesitate and [join the movement](/movement/about-us).

## Free software development today

Modern free software development is very complex, and puts a high burden on any free software developer starting a new project. The project's success depends on their ability and skills, available resources and dedicated commitment to see their work through to completion and then continue to maintain it.

On top of the complexity inherent in any IT project there are many additional factors that contribute to success or failure of free software. There's the often-mentioned ["Tragedy of the Commons"](https://en.wikipedia.org/wiki/Tragedy_of_the_commons), and in general a tension exists between technical teams or individuals - often volunteers - and the people using their software. As a result many projects do not reach their full potential or fail altogether. Developing free software can be an unrewarding and unthankful job. Various factors contribute to developers and maintainers losing interest, or even burning out.

The Social Coding Movement will delve deeper into failure modes and critical success factors and how to get best results.

Let's briefly explore some of the pain points that will be addressed.

### The developer perspective

Common pitfalls for the project maintainers and developers are:

- **Overly technical focus**: Technical considerations absorb most available time.
- **Business myopia**: Undervaluing importance of  non-technical organization processes.
- **Lack of delegation**: Having too much authority, responsibilities and tasks.
- **Undervalued soft skills**: Unclear communication, finding consensus, not inclusion.
- **No financial support**: Volunteering becomes unattractive as obligations and workload grow.

### Other people involved

While overburdened developers and maintainers struggle to keep pace, the people using their work start to negatively affect the sustainability of the project as well. Some forces that play a role are:

- [**False entitlement**](/methodology/patterns/challenges/entitlement): "Free beer" mentality. People feel invested, demand support and features.
- **Unrealistic expectations**: Having strong opinion on project direction beyond its scope.
- **Unfair judgment**: Expressing very negative sentiments on any perceived shortcoming.
- **Lack of feedback**: Using, but not giving back to voice their impression and needs.
- **Unable to get involved**: High barrier to provide feedback, lack of easy ways to do so.

Taken together all these factors that lead to project failure give free software development an undeserved bad name and reputation with the broader public. This while there are many successful projects out there that are widely used and loved today. Our mission is to grow that number by handing the tools and practices to do so.

## Why we need Social Coding

Problems and solutions for a healthy free software movement are heavily discussed all the time. Much has been written and many initiatives exist to help bring improvement. All this activity has something in common: It is the interaction between people, their collaboration and collective efforts that sets free software development apart.

{: .note-title}
> Social Coding
>
> Social coding recognizes that free software development is first and foremost a social process.

In order to improve free software and its development we need to embrace these social aspects. Recognizing the needs of people, listening to all voices and allowing everyone to participate along the way. This in practice is quite hard.

Our Social Coding initiative explores [**Common challenges**](/methodology/patterns/challenges), and we collect the many [**Best-practices**](/methodology/patterns/best-practices) that are around, so that we can build better together.

The Social Coding Movement consists of people that investigate all roads that lead to a free software project's long-term sustainability.

### Community comes first

The core realization of Social Coding is that free software cannot be developed without establishing a diverse community to support it. For this reason Social Coding encourages formation of a vibrant Community alongside the organization of the Project itself. Software projects that rely on a community can mitigate a lot of the risks that a development process driven by lone developers bring with it. But community building is challenging task.

### Democratic governance

Having just a community is not enough. Its members have to be able to govern it together. They must coordinate and delegate responsibilities, resolve conflicts, mediate discussion and make decisions that are supported by the community as a whole. Establishing healthy Communities of Action is one of the important goals of Social Coding.

### From ideas to projects

Social aspects are important throughout the _entire_ free software development lifecycle. Social coding starts with ideation. People publish ideas, interact on them with others, register their interest to form a community of people. Then they move on to create projects that are managed and supported by the community, and become long-term sustainable efforts.

### Leveraging the Fediverse

Recent years saw the rise of the [Fediverse](https://joinfediverse.wiki/index.php?title=Main_Page), a 'social fabric' where many people, or _fedizens_, interact using a variety of different applications. The Fediverse is furtile ground for Social Coding practitioners. With its open and diverse culture and a broadly shared passion for free software initiatives it is an ideal medium to support the social aspects of software development.

### Build the tools we need

The entire software development process lends itself to automation to help support our tasks. Numerous free software tools already exist. We will use them where we can. However, when it comes to social networking, the Fediverse offers opportunity that goes beyond what current tools offer. Starting simple, gradually we will support more social aspects of the free software development lifecycle.

Our Ecosystem projects will include federated applications we build together. Such as the [**Ideation Hub**](/ecosystem/projects/ideationhub). A tool that allows any fedizen to brainstorm their ideas, elaborate them together, form a community of interested peers, and spin off a free software project to realize them.

## Join our community

By now it should be very clear. We can't do this all alone. It doesn't matter if you are technical or not, what your background is or your skills. We need your input and your help. So please participate in our channels, give your feedback, raise your voice.

We love all your contributions. [**Join the movement !**](/movement/about-us)
