---
layout: default
title: Publish to this site
nav_order: 70
permalink: /movement/publish-howto
---

# How you can contribute to the Social Coding website
{: .fs-9 }

Social Coding is a movement we must create collaboratively. That includes collecting your valuable knowledge and experiences. We 💜 love your contributions.
{: .fs-6 .fw-300 }

[Get started now](/movement/publish-howto/configuration#getting-started){: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 .mr-2 } [View it on Codeberg](https://codeberg.org/SocialCoding/website){: .btn .fs-5 .mb-4 .mb-md-0 }

---

## Structure of the website

Social Coding is a crowdsourced initiative. All pages on this site are candidates for continual improvement. But some sections specifically are where we want to gather your knowledge and insights:

- [**Challenges**:](/methodology/patterns/challenges) Add sub-pages to specific issues that plague free software development, and a summary in the top-level page. Before doing so they should be [discussed](https://codeberg.org/SocialCoding/discussion/issues) first and have consensus for their addition.

- [**Patterns and practices**:](/methodology/patterns/best-practices) Same as challenges, we add best-practices and commons solutions, gradually creating a knowledge base. Also discuss new sub-pages before writing them.

- [**Practitioners guide**:](/methodology/practitioners/) Here the entire methodology of Social Coding is explained in a manual format that contains cross-references to other site sections and choices to be made. It is like a cookbook with recipes, but in single-page format.

- [**IdeationHub documentation**:](/ecosystem/projects/ideationhub/design) Both the design and implementation will evolve based on both technical and non-technical discussion that takes place in the various repositories that are related to this sub-project. We will practice [README-driven development](https://tom.preston-werner.com/2010/08/23/readme-driven-development.html) to ensure it is kept up-to-date.

- [**Blog and news**:](/movement/blog) Your blog articles are most welcome and we gladly publish them, but please discuss them first before doing so. News and announcements will be posted by the Social Coding maintainers.

## Code of Conduct

We are committed to fostering a welcoming community.

[View our Code of Conduct](https://codeberg.org/SocialCoding/website/src/branch/main/CODE_OF_CONDUCT.md) on our Codeberg repository.
